#!/usr/bin/python

from ase import Atoms
from ase.io import read, write
import numpy as np
from ase.units import Bohr
import os

#Initialize
list = []; vib_list = []; linenum = []
index = []; set1 = []; set2 = []; set3 =[]
count = 0
open('vibrations.mol','w')

#Get total num of atoms
atoms = read('a.xyz') 
natoms = atoms.get_number_of_atoms()

#Get total number of relaxed atoms
for i in open('in','r'):
    if 'LIST' in i:
        l1 = i
nfreeze = len(l1.strip().split()[1:])
nrelax = natoms - nfreeze
print "Number of relaxed atoms = %d (= natoms (%d) - nfrozen atoms (%d))" %(nrelax,natoms,nfreeze) 
#nrelax = int(raw_input("Enter the number of relaxed atoms: "))

#Reading all the vibrational frequencies
with open('out', 'r') as f:
  for num, line in enumerate(f):
    if 'VIB|Frequency (cm^-1)' in line:
      a,b,c = line.strip().split()[2:]
      vib_list.append(float(a))
      vib_list.append(float(b))
      vib_list.append(float(c))
      linenum.append(num) 
print "Number of Vibration frequencies (both positive and negative) found are: ", len(vib_list)

#Write the vibrational frequencies to vibrations.mol
with open('vibrations.mol','a') as f:
  f.write(' [Molden Format]\n [FREQ]\n')
  for i in range(len(vib_list)):
    print >> f, vib_list[i]
  f.write(' [FR-COORD]\n')

#Write the Bohr coordinates in to vibrations.mol 
atoms_dup = atoms
atoms_bohr = atoms_dup.get_positions() / Bohr
atoms_dup.set_positions(atoms_bohr)
with open('vibrations.mol','a') as f:
  for i in atoms_dup:
    print >> f, "%s  %.7f  %.7f  %.7f" %(i.symbol, i.x, i.y, i.z)
  f.write(' [FR-NORM-COORD]\n')
  
lineshift = 4   #To go to lines containing the displacements
nplinenum = np.array(linenum) + lineshift

with open('out', 'r') as f:
  lines = f.readlines()
#Go to each line holding the frequencies in cp2k output file
  for m, k in enumerate(nplinenum):
    for num, line in enumerate(lines):
#Select the nrelax lines having the displacements
      if nplinenum[m] < num+lineshift <= nplinenum[m]+nrelax:
#Seperate the data of three sets of vib freqs per line into a, b, c
#        print num+lineshift
        ind, a, b, c = lines[num+lineshift].strip().split()[0], lines[num+lineshift].strip().split()[2:5], lines[num+lineshift].strip().split()[5:8], lines[num+lineshift].strip().split()[8:11]
        fa, fb, fc = [float(i) for i in a], [float(i) for i in b], [float(i) for i in c]
        index.append(int(ind))
        set1.append(fa)
        set2.append(fb)
        set3.append(fc)
#Build an empty system with xyz coordinates for natoms number of atoms  
    zeros = np.zeros(natoms*3)
    zerosxyz = np.reshape(zeros, (-1, 3))
    atoms_disp = Atoms('Ti%d'%natoms, positions=zerosxyz)
#Write the displacemetns to the .mol file for each set1, set2, set3
    with open('vibrations.mol','a') as f:
      print >> f, "vibration      %d" %(count+1) 
    for i, j in zip(index, set1):
      atoms_disp.positions[i-1] = j  #-1 since i is CP2K indexing of atoms
    with open('vibrations.mol', 'a') as f:
      for i in atoms_disp:
        print >> f, '%.2f  %.2f  %.2f' %(i.x, i.y, i.z)
    atoms_disp = Atoms('Ti%d'%natoms, positions=zerosxyz)
    with open('vibrations.mol','a') as f:
      print >> f, "vibration      %d" %(count+2) 
    for i, j in zip(index, set2):
      atoms_disp.positions[i-1] = j 
    with open('vibrations.mol', 'a') as f:
      for i in atoms_disp:
        print >> f, '%.2f  %.2f  %.2f' %(i.x, i.y, i.z)
    atoms_disp = Atoms('Ti%d'%natoms, positions=zerosxyz)
    with open('vibrations.mol','a') as f:
      print >> f, "vibration      %d" %(count+3) 
    for i, j in zip(index, set3):
      atoms_disp.positions[i-1] = j  
    with open('vibrations.mol', 'a') as f:
      for i in atoms_disp:
        print >> f, '%.2f  %.2f  %.2f' %(i.x, i.y, i.z)
    count += 3 
    print "Vibration number %d written" %count
os.system('scp vibrations.mol mac:/Users/siyemperumal/Documents/structs/new')
